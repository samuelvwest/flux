// Utility functions & variables
function arrayUnique(array) { // Compare for unique arrays
    var a = array.concat();
    for(var i=0; i<a.length; ++i) {
        for(var j=i+1; j<a.length; ++j) {
            if(a[i] === a[j])
                a.splice(j--, 1);
        }
    }
    return a;
};
function pad(str, max) { // Turn a number into a string with specified length
    str = str.toString();
    return str.length < max ? pad("0" + str, max) : str;
}
// Day of Week Lookup Array
window._dayOfWeek = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
// Day of Week Lookup Array
window._months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
// Random key generator
function ckey(pref) { 
    function rC() {
        var char = '';
        if (Math.floor(1 + Math.random() * 2) === 1) {
            char = String.fromCharCode(Math.floor(65 + Math.random() * 23));
        } else {
            char = String.fromCharCode(Math.floor(97 + Math.random() * 23));
        }
        return char;
    }
    function sC(p) {
        var ol = 24,
            init = Math.floor((1 + Math.random()) * 0x10000) * Date.now(),
            iS = init.toString(),
            iCN = Math.floor(12 + Math.random() * 42);
        if (p === 'long') {
            ol = 6;
        } else if (p === parseInt(p, 10)) {
            ol = p;
        }
        for (i = 0; i < iCN; i++) {
            var iL = iS.length,
                iP = Math.floor(1 + Math.random() * iL);
            iS = [iS.slice(0, iP), rC(), iS.slice(iP)].join('');
        }
        sub = Math.floor(1 + Math.random() * (iS.length - ol));
        use = iS.substring(sub,sub + ol);
        return use;
    }
    if (pref === 'long') {
        return sC(12) + '_' + sC(12) + '_' + sC(6) + sC(12) + sC(6);
    } else if (pref === parseInt(pref, 10)) {
        var fin = '',
            rem = pref % 24,
            rep = (pref - rem) / 24;
        for (i2 = 0; i2 < rep; i2++) {
            fin += sC();
        }
        if (rem > 0) {
            fin += sC(rem);
        }
        return fin;
    }
    return sC(pref);
};
// Create a clean string with only lowercase numbers and letters and a single dash for all sequences of special characters and spaces.
function simplifyString(str) {
    return str.replace(/[^\w]/gi, '-').replace(/-{1,}/g, '-').toLowerCase();
};




// Create Dynamic Working Task list;
function createWorkingTasksObj() {
    window.wTasks = jQuery.extend(true, {}, window.tasks);
    for (var task in window.wTasks) {
        var tT = window.wTasks[task],
            aA = tT['availability'],
            startStr = new Date(aA[0][0],aA[0][1],aA[0][2],aA[0][3],aA[0][4]),
            startNum = startStr.getTime(),
            endStr = new Date(aA[1][0],aA[1][1],aA[1][2],aA[1][3],aA[1][4]),
            endNum = endStr.getTime(),
            totalMinutesAvailable = (endNum - startNum) / 60000,
            fixedTest = tT.minutes === totalMinutesAvailable;
//                console.log('start: ' + startNum);
//                console.log('end: ' + endNum);
//                console.log(fixedTest + ' : ' + totalMinutesAvailable + ' : ' + tT.minutes);
//                console.log(tT);
        tT['availability'][0][5] = startNum;
        tT['availability'][1][5] = endNum;
        tT['fixed'] = fixedTest;
        tT['dependantMinutes'] = tT.minutes;
        tT['minutesRemaining'] = {
            total: 0,
            continuous: 0
        };
        tT['dependants'] = [];
        tT['timesScheduled'] = [];
        tT['scheduled'] = false;
//        console.log(task);
        propogateDependants(task);
//        for (var pr in tT.prerequisites) {
//            var tPR = tT.prerequisites[pr]
//            console.log('...' + tPR);
//        };
    };
    for (var task in window.wTasks) {
        var tT = window.wTasks[task];
        propogatePrerequisits(task);
        for (var dependant in tT['dependants']) {
            tT.dependantMinutes += window.wTasks[tT['dependants'][dependant]]['minutes'];
//            console.log(tT['dependants'][dependant]);
        }
    };
};
function propogateDependants(task,dependants) {
    var tT = window.wTasks[task],
        prerequisites = tT['prerequisites'],
        dependants = dependants ? dependants : [];
//    console.log(tT.name);
//    console.log('Dependants: ' + dependants.length);
    for (var dependant in dependants) {
        var tD = dependants[dependant];
        if (tT.dependants.indexOf(tD) === -1) {
            tT.dependants.push(tD);
        };
    };
//    console.log(tT.dependants);
    dependants.push(task);
    for (var prereq in prerequisites) {
        propogateDependants(prerequisites[prereq],dependants);
    };
};
function propogatePrerequisits(task) {
    var tT = window.wTasks[task];
//    console.log(tT);
    for (var dependant in tT.dependants) {
        var tD = tT.dependants[dependant],
            wDT = window.wTasks[tD],
            wDTP = window.wTasks[tD].prerequisites;
//        console.log(wDT.name);
//        console.log(wDTP.indexOf(task));
////        console.log(wDT.prerequisites.indexOf(tD) + ' : ' + (wDT.prerequisites.indexOf(tD) === -1));
        if (wDTP.indexOf(task) === -1){
            wDTP.push(task);
        };
    };
};

// Returns Task div Element
function createTaskElement(taskId) {
    var taskObj = window.wTasks[taskId],
        tAv = taskObj.availability,
        startTime = new Date(tAv[0][0],tAv[0][1],tAv[0][2],tAv[0][3],tAv[0][4]),
        endTime = new Date(tAv[1][0],tAv[1][1],tAv[1][2],tAv[1][3],tAv[1][4]),
        contexts = function() {
            var contextElems = '';
            for (ci = 0; ci < taskObj.contexts.length; ci++) {
                contextElems += '<span>' + taskObj.contexts[ci] + '</span>';
            }
            return contextElems;
        },
        listElem = function(type) {
            var listElems = '';
            for (var prereq in taskObj[type]) {
                name = window.wTasks[taskObj[type][prereq]]['name'];
                listElems += '<li>' + name + '</li>';
            }
            return listElems;
        },
        taskElem = '\
        <div class="task" id="' + taskId + '">\
            <div class="id">' + taskId + '</div>\
            <div class="contexts">\
                ' + contexts() + '\
            </div>\
            <div class="name">' + taskObj.name + '</div>\
            <div class="hours">hours: ' + (taskObj.minutes / 60) + '</div>\
            <div class="hours">dependant hours: ' + (taskObj.dependantMinutes / 60) + '</div>' +
//            '<div class="dateTime">start: ' + tAv[0][1] + '/' + tAv[0][2] + '/' + tAv[0][0] + ' ' + tAv[0][3] + ':' + tAv[0][4] + '</div>' +
            '<div class="dateTime">start time: ' + startTime + '</div>' +
            '<div class="dateTime">start milliseconds: ' + startTime.getTime() + '</div>' +
//            '<div class="dateTime">end: ' + tAv[1][1] + '/' + tAv[1][2] + '/' + tAv[1][0] + ' ' + tAv[1][3] + ':' + tAv[1][4] + '</div>' +
            '<div class="dateTime">end: ' + endTime + '</div>' +
            '<div class="dateTime">end milliseconds: ' + endTime.getTime() + '</div>';
    if (Object.keys(taskObj.prerequisites).length > 0 || Object.keys(taskObj.dependants).length > 0) {
        taskElem += '\
            <div class="relationships">\
                <ul class="prerequisites">Prerequisites\
                    ' + listElem('prerequisites') + '\
                </ul>\
                <ul class="dependants">Dependants\
                    ' + listElem('dependants') + '\
                </ul>\
            </div>';
    };
    taskElem += '\
        </div>';
    return(taskElem);
};








// Create Dynamic Working Context list;
function createWorkingContextsObj() {
    window.wContexts = jQuery.extend(true, {}, window.contexts);
    for (var context in window.wContexts) {
        window.wContexts[context]['minutesRemaining'] = 0;
        window.wContexts[context]['tasks'] = {};
        for (var task in window.wTasks) {
            if (window.wTasks[task].contexts.indexOf(context) > -1) {
                window.wContexts[context].tasks[task] = window.wTasks[task];
            };
        };
    };
};
createWorkingContextsObj();

// Returns Task div Element
function createContextElement(contextId) {
    var contextObj = window.wContexts[contextId],
        fromTo = function(availabilityObj){
            
        },
        contextElem = '\
        <div class="context" id="context_' + contextId + '" style="border-color: ' + contextObj.color + ';">\
            <div class="id">' + contextId + '</div>\
            <div class="name">' + contextObj.name + '</div>\
            <div class="color">color: <span style="color:' + contextObj.color + ';">' + contextObj.color + '</span></div>\
            <div class="availability">';
    for (var dayOfWeek in contextObj.available) {
        contextElem += '\
                <div class="dayOfWeek ' + _dayOfWeek[dayOfWeek] + '">';
        for (var range in contextObj.available[dayOfWeek]) {
            var rObj = contextObj.available[dayOfWeek][range];
            if (range > 0) {
                contextElem += '\
                    <span class="and">and</span>';
            };
            contextElem += '\
                    <span class="range">' + pad(rObj[0],2) + ':' + pad(rObj[1],2) + ' - ' + pad(rObj[2],2) + ':' + pad(rObj[3],2) + '</span>';
        }
        contextElem += '\
                </div>';
    };
    contextElem += '\
            </div>';
    
    if (Object.keys(contextObj.tasks).length > 0) {
        contextElem += '\
            <ul class="tasksWithContext">';
        for (var task in contextObj.tasks) {
            contextElem += '\
                <li>' + contextObj.tasks[task].name + '</li>';
        };
        contextElem += '\
            </ul>';
    };
    contextElem += '\
        </div>';
    return(contextElem);
};

//        id: 'personal',
//        name: 'Personal',
//        color: '#253cba',
//        minutesRemaining: 0,
//        available: [
//            [
//                [9,0,20,30]
//            ], [
//                [5,0,8,30],
//                [17,30,20,30]
//            ], [
//                [5,0,8,30],
//                [17,30,20,30]
//            ], [
//                [5,0,8,30],
//                [17,30,20,30]
//            ], [
//                [5,0,8,30],
//                [17,30,20,30]
//            ], [
//                [5,0,8,30],
//                [17,30,22,0]
//            ], [
//                [9,0,22,0]
//            ]
//        ]








// Creates Time Block data object 
function createTimeBlockDataObject(now) {
    var nowStr = new Date(now),
        nowYear = nowStr.getFullYear(),
        nowMonth = nowStr.getMonth(),
        nowDay = nowStr.getDay(),
        nowHour = nowStr.getHours(),
        nowMinute = nowStr.getMinutes(),
        nowDateTimeStr = pad(nowYear,4) + pad(nowMonth,2) + pad(nowDay,2) + pad(nowHour,2) + pad(nowMinute,2),
        nowObj = {
            dStr: new Date(now),
            dNum: now,
            datetime: nowDateTimeStr,
            contexts: {},
            tasks: {}
        };
    // Get applicable contexts
    for (var context in contexts) {
        var availableRanges = contexts[context]['available'][nowDay],
            i = 0;
        for (var range in availableRanges) {
            var sH = availableRanges[range][0],
                sM = availableRanges[range][1],
//                sT1 = sH < nowHour ? true : false,
//                sT2 = sH === nowHour && sM <= nowMinute ? true: false,
                sT = sH < nowHour ? true : sH === nowHour && sM <= nowMinute ? true: false,
                eH = availableRanges[range][2],
                eM = availableRanges[range][3],
//                eT1 = eH > nowHour ? true : false,
//                eT2 = eH === nowHour && eM > nowMinute ? true : false,
                eT = eH > nowHour ? true : eH === nowHour && eM > nowMinute ? true : false,
                fT = sT && eT;
            if (fT) {
                nowObj.contexts[context] = {};
            };
            i++;
        };
    };
    return(nowObj);
};

// Create Flux Calendar array
function createFluxCalendarArray(now,end,timeBlockMinutes) {
    // Create Flux Calendar Baseline
    window.fluxCal = [];
    var timeBlockMilliseconds = timeBlockMinutes * 60000;
    while (now < end) {
        window.fluxCal.push(createTimeBlockDataObject(now));
        now = now + timeBlockMilliseconds;
//                console.log(cNObj);
    };
    // Update Flux Calendar with time left in various attributes
    for (i = window.fluxCal.length - 1; i >= 0; i--) {
        var timeBlock = window.fluxCal[i],
            ctxts = timeBlock['contexts'],
            newContextsObj = {};
//        console.log(ctxts);
//        console.log(timeBlock);
        // Update Flux Calendar with time left for each task
        for (var task in window.wTasks) {
            var tObj = window.wTasks[task],
                t1 = false,
                t2 = tObj.fixed;
            for (var context in tObj.contexts) {
                var cTest = ctxts[tObj.contexts[context]]?true:false;
//                console.log(context);
                if (cTest) {
                    t1 = true;
                    break;
                }
            };
            if (t1 || t2) {
                var tBEnd = timeBlock.dNum + timeBlockMilliseconds,
                    aT1 = tObj.availability[0][5] <= timeBlock.dNum,
                    aT2 = tObj.availability[1][5] >= tBEnd;
                if (aT1 && aT2) {
                    var totalMinutesRemainingInTask = tObj.minutesRemaining.total + timeBlockMinutes,
                        continuousMinutesRemainingInTask = tObj.minutesRemaining.continuous + timeBlockMinutes;
                    tObj.minutesRemaining = {
                        total: totalMinutesRemainingInTask,
                        continuous: continuousMinutesRemainingInTask
                    };
                    timeBlock.tasks[task] = jQuery.extend(true, {}, tObj);
//                    console.log('%c' + task + ' : ' + totalMinutesRemainingInTask + ' : ' + continuousMinutesRemainingInTask, 'color: #419a41');
                } else {
                    tObj.minutesRemaining.continuous = 0;
//                    console.log('%c' + task + ' : wrong time : ' + aT1 + ' : ' + aT2 + ' : ' + tObj.minutesRemaining.total, 'color: #ad2c2c');
                }
            } else {
                tObj.minutesRemaining.continuous = 0;
//                console.log('%c' + task + ' : wrong context : ' + ctxts + ' : ' + tObj.minutesRemaining.total, 'color: #ad2c2c');
            }
        };
        console.log(timeBlock);
    };
    // Deduce what task should be scheduled in this Time Block
    for (i = 0; i < window.fluxCal.length; i++) {
        var tTB = window.fluxCal[i],
            pTB = window.fluxCal[i-1],
            tTBTs = tTB.tasks,
            notScheduled = {},
            fixed = {},
            currentlyScheduling = {},
            allPreqsDone = {};
//        console.log(tTB);
//        console.log('TimeBlock Tasks : ' + Object.keys(tTBTs).length);
        if (Object.keys(tTBTs).length > 0) {
            for (var task in tTBTs) {
                var gTObj = window.wTasks[task],
                    tTObj = tTBTs[task];
                if (!gTObj['scheduled']) { // Already Scheduled?
                    notScheduled[task] = tTObj;
                    if (gTObj.fixed) { // Fixed Placement
                        fixed[task] = tTObj;
                    };
                    var cTBSTest = window.currentTasksBeingScheduled[task]?true:false;
                    if (cTBSTest) { // Check if something is currently in Active Scheduling
                        currentlyScheduling[task] = tTObj;
                    };
                    var prereqsDone = true;
                    for (var prereq in gTObj.prerequisites) {
                        if (!window.wTasks[gTObj.prerequisites[prereq]]['scheduled']) {
                            prereqsDone = false;
                            break;
                        }
                    }
//                    console.log(prereqsDone);
                    if (prereqsDone) {
                        allPreqsDone[task] = tTObj;
                    }
                };
            };
//            console.log('Not Scheduled : ' + Object.keys(notScheduled).length);
//            console.log('Fixed : ' + Object.keys(fixed).length);
//            console.log('Currently Scheduling : ' + Object.keys(currentlyScheduling).length);
//            console.log('All Prereqs Done : ' + Object.keys(allPreqsDone).length);
            if (Object.keys(allPreqsDone).length > 0) {
//                var taskAlreadyBeingScheduled;
//                if (window.currentTasksBeingScheduled.length)
                var tasksToPrioritize = Object.keys(fixed).length !== 0 ? fixed : Object.keys(currentlyScheduling).length !== 0 ? currentlyScheduling : allPreqsDone,
                    tasksRestraintsArr = [];
                for (var task in tasksToPrioritize) {
                    var gTObj = window.wTasks[task],
                        tTObj = tasksToPrioritize[task],
                        timeRestraintRatio = gTObj.dependantMinutes / tTObj.minutesRemaining.total;
                    tTObj['timeRestraintRatio'] = timeRestraintRatio
                    tasksRestraintsArr.push(timeRestraintRatio);
//                    console.log(task + ' : ' + tTObj.timeRestraintRatio);
//                    if (gTObj.fixed) {
//                        fixed[task] = tTObj;
//                    };
                };
                var maxTimeRestraint = Math.max(...tasksRestraintsArr),
                    possibleTasksArr = [];
                for (var task in tasksToPrioritize) {
                    var gTObj = window.wTasks[task],
                        tTObj = tasksToPrioritize[task];
                    if (tTObj.timeRestraintRatio === maxTimeRestraint) {
                        possibleTasksArr.push(task);
                    }
                };
//                console.log(possibleTasksArr);
                var taskToSchedule = possibleTasksArr[0];
                if (Object.keys(currentlyScheduling).length !== 0) {
                    var tTSObj = currentTasksBeingScheduled[taskToSchedule];
                } else {
                    var tTSObj = jQuery.extend(true, {}, tasksToPrioritize[taskToSchedule]);
                };
                tTSObj.id = taskToSchedule;
                tTSObj.minutes = tTSObj.minutes - timeBlockMinutes;
                tTSObj.dependantMinutes = tTSObj.dependantMinutes - timeBlockMinutes;
//                console.log(tTSObj.minutes);
                if (tTSObj.minutes <= 0) {
                    window.wTasks[taskToSchedule]['scheduled'] = true;
                    delete currentTasksBeingScheduled[taskToSchedule];
//                    console.log('Removed Task from current : ' + taskToSchedule);
                } else {
                    currentTasksBeingScheduled[taskToSchedule] = tTSObj;
//                    console.log('Added/Modified task in current : ' + taskToSchedule);
//                    console.log(tTSObj);
                }
                tTB['scheduledTask'] = tTSObj;
//                tTSObj.minutes
            };
        };
//        var assignedTaskTest = tTB.scheduledTask ? true : false;
//        if (assignedTaskTest) {
//            console.log(tTB.scheduledTask.name);
//        };
        // Set whether this is the beggining, middle, or end of a time block
        tTB['blockType'] = [];
        if (i > 0) {
            var thisTask = tTB.scheduledTask ? tTB.scheduledTask.id : 'empty',
                previousTask = pTB.scheduledTask ? pTB.scheduledTask.id : 'empty';
            if (thisTask !== 'empty') {
                tTB.blockType.push('middle');
            };
            if (thisTask !== previousTask) {
                if (thisTask !== 'empty') {
                    tTB.blockType.push('start');
                    window.wTasks[tTB.scheduledTask.id].timesScheduled.push([tTB.dStr]);
//                    console.log('start : ' + tTB.scheduledTask.id);
//                    console.log(tTB);
                };
                if (previousTask !== 'empty') {
                    pTB.blockType.push('end');
                    gPT = window.wTasks[pTB.scheduledTask.id].timesScheduled;
                    gPT[gPT.length - 1].push(tTB.dStr);
//                    console.log('end : ' + pTB.scheduledTask.id)
//                    console.log(pTB);
                };
            };
        } else {
            if (typeof tTB.scheduledTask !== 'undefined') {
                tTB['blockType'].push('start');
            };
//            console.log(tTB.scheduledTask.name + ' : ' + tTB.scheduledTask ? 'start' : 'empty');
        };
    };
};


// Returns Time Block div Element
function createTimeBlockElement(timeBlockObj) {
    var month = timeBlockObj.dStr.getMonth(),
        day = timeBlockObj.dStr.getDate(),
        hour = timeBlockObj.dStr.getHours(),
        minute = timeBlockObj.dStr.getMinutes(),
        contexts = '',
        tasks = '',
        scheduledTask = timeBlockObj.scheduledTask ? timeBlockObj.scheduledTask.name : '',
        blockTypeArr = timeBlockObj.blockType,
        blockType = blockTypeArr.length === 0 ? 'empty' : '';
    for (var bT in blockTypeArr) {
        blockType += blockTypeArr[bT] + ' ';
    };
    for (var context in timeBlockObj.contexts) {
        contexts += '<div class="context ' + context + '"></div>';
    };
//    var timeBlockElem = '\
//        <div class="timeBlock">\
//            <span class="date">' + timeBlockObj.dStr + '</span>\
//            <span class="contexts"> ' + contexts + '</span>\
//            <span class="tasks"> ' + Object.keys(timeBlockObj.tasks).length + '</span>\
//            <span class="scheduledTask"> ' + scheduledTask + '</span>\
//        </div>';
    var timeBlockElem = '\
        <div class="timeBlock" block-type="' + blockType + '" hour="' + pad(hour,2) + '" start-minute="' + pad(minute,2) + '">\
            ' + contexts + '\
            <span class="scheduledTask">' + scheduledTask + '</span>\
        </div>';
    return(timeBlockElem);
};





// Set Calendar Styles 
function setCalendarStyles(totalDays) {
    var calHeight = $(window).height() - 45,
        timeBlockHeight = (calHeight - 52) * (timeBlockMinutes / 1440),
        dayWidth = $(window).width() / 7,
        calWidth = dayWidth * totalDays,
        thesContexts = '',
        ci = 1;
    for (var ctxt in window.wContexts) {
        thesContexts += '\
        .timeBlock .context.' + ctxt + ' {\
            left: ' + (ci *2) + 'px;\
            background-color: ' + window.wContexts[ctxt].color + ';\
            }';
        ci++;
    }
    var timeBlockLeftPadding = (ci * 2) + 4,
        calStyles = '\
        <style id="calStyles">\
            #mainContent {\
                width:' + $(window).width() + 'px;\
                height:' + calHeight + 'px;\
                }\
            #calendar {\
                height:' + calHeight + 'px;\
                width:' + calWidth + 'px;\
                }\
            .day {\
                width:' + dayWidth + 'px;\
                }\
            .timeBlock {\
                height:' + timeBlockHeight + 'px;\
                padding-left:' + timeBlockLeftPadding + 'px;\
                }' + thesContexts + '\
        </style>';
    $('#calStyles').replaceWith(calStyles);
};