// Setup Nav Actions
$('#taskOptions').click(function(){
    if ($('#tasks').hasClass('shown')) {
        $('#tasks').removeClass('shown');
    } else {
        $('#tasks').addClass('shown');
        $('#contexts').removeClass('shown');
    };
});
$('#contextOptions').click(function(){
    if ($('#contexts').hasClass('shown')) {
        $('#contexts').removeClass('shown');
    } else {
        $('#contexts').addClass('shown');
        $('#tasks').removeClass('shown');
    };
});
$('#calendarSettings').click(function(){
    $('#tasks').removeClass('shown');
    $('#contexts').removeClass('shown');
});

// List inputs
function displayCurrentListedTasks() {
    createWorkingTasksObj();
    var taskListingsHtml = '';
    for (var task in window.wTasks) {
        taskListingsHtml += createTaskElement(task);
    };
    $('#listedTasks').html(taskListingsHtml);
}
//$('.listTasks').click(function(){
//    displayCurrentListedTasks();
//});
displayCurrentListedTasks();

function displayCurrentListedContexts() {
    createWorkingContextsObj();
    var contextListingsHtml = '',
        addTaskContextsList = '';
    for (var context in window.wContexts) {
        contextListingsHtml += createContextElement(context);
        addTaskContextsList += '<div class="checkbox"><input type="checkbox" name="contexts" value="' + context + '" /><label>' + context + '</label></div>';
    };
    $('#listedContexts').html(contextListingsHtml);
    $('#newTaskContexts').html(addTaskContextsList);
}
displayCurrentListedContexts();
//$('.listTasks').click(function(){
//    displayCurrentListedTasks();
//});
//displayCurrentListedContexts();

// NEW TASK
function getCheckedItems(parentId) {
    var checkedArray = [];
    for (i = 0; i < $('#' + parentId + ' .checkbox > input:checked').length; i++) {
        checkedArray.push($('#' + parentId + ' .checkbox > input:checked').eq(i).attr('value'));
    };
//    console.log(checkedArray);
    return checkedArray;
};
$('.addCheckbox').click(function(e){
    e.preventDefault();
    var checkboxNames = $(this).attr('checkbox-name'),
        valToAdd = $(this).next().val(),
        taskName = wTasks[valToAdd].name;
    $(this).parent().append('<div class="checkbox"><input type="checkbox" name="' + checkboxNames + '" value="' + valToAdd + '" checked /><label>' + taskName + '</label></div>');
});
$('#newTask').submit(function(e){
    e.preventDefault();
    var id = 'task_' + ckey(),
        sA = $('#newTask [name="start-availability"]').val().split(/[-T:]+/),
        eA = $('#newTask [name="end-availability"]').val().split(/[-T:]+/),
        availabilityArrays = [
             [parseFloat(sA[0]),parseFloat(sA[1]) - 1,parseFloat(sA[2]),parseFloat(sA[3]),parseFloat(sA[4])],
             [parseFloat(eA[0]),parseFloat(eA[1]) - 1,parseFloat(eA[2]),parseFloat(eA[3]),parseFloat(eA[4])],
        ],
        newTask = {
            'name': $('#newTask [name="name"]').val(),
            'projects': getCheckedItems('newTaskProjects'),
            'contexts': getCheckedItems('newTaskContexts'),
            'minutes': parseFloat($('#newTask [name="minutes"]').val()),
            'availability': availabilityArrays,
            'prerequisites': getCheckedItems('newTaskPrerequisites')
        };
//    console.log(newTask);
    window.tasks[id] = newTask;
    displayCurrentListedTasks();
    $('#newTask #newTaskPrerequisites .checkbox').each(function(){
        $(this).remove();
    });
    $('#listedTasks').scrollTop($('#' + id).offset().top);
});

// NEW CONTEXT
$('#newContext').submit(function(e){
    e.preventDefault();
    var name = $('#newContext [name="name"]').val(),
        contextObj = {
            id: simplifyString(name),
            name: name,
            color: $('#newContext [name="color"]').val(),
            available: []
        };
        for (var day in _dayOfWeek) {
            var dayStr = _dayOfWeek[day].substring(0,3).toLowerCase(),
                rangesCount = $('#newContext [' + dayStr + '-range-num]').length,
                rangesArr = [];
            for (i = 1; i <= rangesCount; i++) {
                var rangeArr = [
                    parseFloat($('#newContext input[name="' + dayStr + '-start-hour-' + i + '"]').val()),
                    parseFloat($('#newContext input[name="' + dayStr + '-start-minute-' + i + '"]').val()),
                    parseFloat($('#newContext input[name="' + dayStr + '-end-hour-' + i + '"]').val()),
                    parseFloat($('#newContext input[name="' + dayStr + '-end-minute-' + i + '"]').val())
                ];
                var t1 = rangeArr[2] > rangeArr[0],
                    t2 = rangeArr[2] === rangeArr[0] && rangeArr[3] > rangeArr[1];
                if (t1 || t2) {
                    rangesArr.push(rangeArr);
                };
            };
            contextObj.available.push(rangesArr);
            console.log(dayStr + ' : ' + rangesCount);
            console.log(rangesArr);
        };
    contexts[contextObj.id] = contextObj;
    displayCurrentListedContexts();
    console.log(contextObj);
});
//  {
//      id: 'sample-id', // Unique context id
//      name: 'Sample Name', // Readable Context title/name
//      available: [[ // 0 Sunday
//              [8,0,16,30]
//          ], [ // 1 Monday
//              [8,0,9,0], // First period available this day [startHour,startMinute,endHour,endMinute]
//              [17,15,20,15] // Second period available this day [startHour,startMinute,endHour,endMinute]
//              // Unlimited Periods can be placed, be carful not to let them overlap
//          ], [ // 2 Tuesday
//              [8,0,9,0],
//              [17,15,20,15]
//          ], [ // 3 Wednesday
//              [8,0,9,0],
//              [17,15,20,15]
//          ], [ // 4 Thursday
//              [8,0,9,0],
//              [17,15,20,15]
//          ], [ // 5 Friday
//              [8,0,9,0],
//              [17,15,22,15]
//          ], [ // 6 Saturday
//              [9,0,22,0]
//          ]
//      ]
//  },

// Setup Calendar section
$('#calendarSettings').click(function(){
    $('#spinner').addClass('show');
    setTimeout(function() {
        createWorkingTasksObj();
        createWorkingContextsObj();
        createFluxCalendarArray(calNow,stopTime,timeBlockMinutes); 
        var startMin = 0,
            totalDays = 0,
            fluxCalendarHtml = '';
        for (var timeBlock in fluxCal) {
            if (startMin === 0){
                var dayStr = fluxCal[timeBlock].dStr,
                    dayBeginning = '\
                    <div class="day ' + _dayOfWeek[dayStr.getDay()] + ' ">\
                        <span class="monthDay">' + _months[dayStr.getMonth()] + ' ' + dayStr.getDate() + '</span>';
                fluxCalendarHtml += dayBeginning;
                totalDays++;
            };
            fluxCalendarHtml += createTimeBlockElement(fluxCal[timeBlock]);
            startMin += timeBlockMinutes;
            if (startMin === 1440) {
                fluxCalendarHtml += '</div>';
                startMin = 0;
            };
        };
        $('#calendar').html(fluxCalendarHtml);
        setCalendarStyles(totalDays);
        $('#spinner').removeClass('show');
        if (typeof window.calendarStyleReplace === 'undefined') {
            window.calendarStyleReplace = true;
            $(window).resize(function(){
                setCalendarStyles();
            })
        };
    },200);
});

$('#mainContent').css({'min-height':($(window).height() - 45).toString() + 'px'});