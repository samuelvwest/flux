global.base_dir = __dirname;
global.abs_path = function(path) {
    return base_dir + path;
}
global.fp = function(file) {
    return abs_path('/' + file);
}

const fs = require('fs')
const path = require('path')
const express = require('express')
const exphbs = require('express-handlebars')
const app = express()
const port = 8080

// Make sure all lib files can be referenced as static content
app.use('/lib', express.static(fp('lib')))

// Setup Handlebars render engine
app.engine('.hbs', exphbs({
    defaultLayout: fp('app/views/layouts/calendar'),
    extname: '.hbs',
    layoutDir: fp('app/views/layouts')
}))
app.set('view engine', '.hbs')
app.set('views', fp('app/views'))

// ROUTES
app.get('/', (request, response) => {
    fluxApp = require('./app/app.js')
    fluxApp.page(express,exphbs,app,request,response)
    var date = new Date()
    console.log('render app at ' + date)
})

// Handle setup errors
app.use((err, request, response, next) => {
    console.log(err)
    response.status(500).send('Something broke!')
})

// Start listening on port
app.listen(port, (err) => {
    if (err) {
        return console.log('something bad happened', err)
    }
    console.log('server is listening on ' + port)
})